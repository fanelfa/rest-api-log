from logharian.views import KegiatanViewSet, KategoriViewSet
from rest_framework import routers

router = routers.DefaultRouter()

router.register('kegiatan', KegiatanViewSet)
router.register('kategori', KategoriViewSet)