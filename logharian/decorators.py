from django.http import JsonResponse

def allowed_users(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            print('Username: {}'.format(request.user.username))
            print(request.user)

            # group = None
            # if request.user.groups.exists():
            #     group = [x.name for x in request.user.groups.all()]
            
            # if group in allowed_roles:
            #     return view_func(request, *args, **kwargs)
            # else:
            #     return JsonResponse({
            #                 'detail':"You don't have permissions to access this!",
            #                 'your name':request.user.username,
            #                 'your group':group
            #     }, status=403)
            return view_func(request, *args, **kwargs)
        return wrapper_func
    return decorator