from django.http import JsonResponse

from django.utils.decorators import method_decorator
# from django.contrib.auth.decorators import user_passes_test
# from django.contrib.auth.models import Group

from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from rest_framework_jwt.authentication import JSONWebTokenAuthentication


from .models import Kegiatan, Kategori
from .serializers import KegiatanSerializer, KategoriSerializer
from .decorators import allowed_users
from .permissions import IsOwnerOnly



# @method_decorator(allowed_users(allowed_roles=['pustakawannn',]), name='dispatch')
class KegiatanViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """

    permission_classes = (IsAuthenticated, IsOwnerOnly)
    authentication_classes = (JSONWebTokenAuthentication, )


    queryset = Kegiatan.objects.all()
    serializer_class = KegiatanSerializer


class KategoriViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated, IsAdminUser, )
    authentication_classes = (JSONWebTokenAuthentication, )


    queryset = Kategori.objects.all()
    serializer_class = KategoriSerializer

# @api_view(['GET'])
# def not_login_user(request):
#     if request.method == 'GET':
#         return JsonResponse({'status':'You need to log in!!'})