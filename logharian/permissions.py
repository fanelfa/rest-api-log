from rest_framework import permissions
# from django.contrib.auth.models import User


class IsOwnerOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        # if request.method in permissions.SAFE_METHODS:
        #     return True

        # Write permissions are only allowed to the owner of the snippet.
               
        return obj.user == request.user

class IsSameGroup(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        
        if request.user.is_superuser:
            return True

        if [g.name for g in obj.user.groups.all()] == [g.name for g in request.user.groups.all()]:
            return True
        else:
            return False