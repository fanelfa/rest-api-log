from django.contrib import admin
from logharian.models import Kategori, Kegiatan

# Register your models here.

class KegiatanAdmin(admin.ModelAdmin):

    fields= ('judul', 'bukti', 'kategori', )
    list_display = ('judul', 'bukti', 'kategori', 'user', 'created_at', 'updated_at')

    def save_model(self, request, obj, form, change): 
        obj.user = request.user
        obj.save()


class KategoriAdmin(admin.ModelAdmin):

    fields= ('nama', 'nilai', )
    list_display = ('nama', 'nilai', 'created_by', 'created_at', 'updated_at')

    def save_model(self, request, obj, form, change): 
        obj.created_by = request.user
        obj.save()

admin.site.register(Kategori, KategoriAdmin)
admin.site.register(Kegiatan, KegiatanAdmin)
