from django.db import models
from django.contrib.auth.models import User

import os
from uuid import uuid4


def user_directory_path(instance, filename):
    # get pembeda
    if instance.pk:
        pembeda = '{}'.format(instance.pk)
    else:
        # set pembeda as random string
        pembeda = '{}'.format(uuid4().hex)
    return 'bukti_kegiatan/{0}/{1}-{2}'.format(instance.user.id, pembeda, filename)



# Create your models here.
class Kategori(models.Model):
    nama = models.CharField(max_length=100, unique=True)
    nilai = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, null=True, on_delete=models.PROTECT)

    def __str__(self):
        return self.nama

class Kegiatan(models.Model):
    judul = models.CharField(max_length=500)
    bukti = models.FileField(upload_to=user_directory_path, blank=True, null=True)
    kategori = models.ForeignKey(Kategori, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.judul